#!/usr/bin/env zsh
#
# (might work with other shells, but I won't support them)
#
# This script maintains a directory structure as follows:
# (base directory)
#	├ config-repo.zsh
#	├ (prefixes.zsh)
#	├ .git
#	├ .gitignore
#	├ contents
#	│	├ fileA
#	│	├ fileB
#	│	└ ...
#	└ symlinks
#		├ fileA
#		├ fileB
#		└ ...
#
# Each of the files in <symlinks> is a symlink to some user/system config file
# (or anything really, but it's intended for config files).
# To maintain privacy, the <symlinks> directory is included in the .gitignore,
# so that it's possible to publish the repo without leaking local paths.
#
# Instead, the files in <contents> have a special format where the first line of the file
# contains a prefix (some variable you can set) and a relative path.
# This approach to only storing local paths keeps everything nice and anonymous
# (and also more portable).
#
# Prefixes are simply set as arguments to this script
# - Valid prefix names are strings made of ascii alphanumeric characters and/or '_'.
# - Valid prefix values are strings starting with '/' (absolute paths).
#
# This script itself has to be in the same directory (also tracked by git) so that it can be shared,
# and doing this also helps add clarity as to what this directory is for and how it works
# (e.g. in case you're revisiting it after 6 months and have forgotten about it in the meantime).
#
# Optionally, you can have the <prefixes.zsh> file set the prefixes, so you don't have to set them manually.
# That file is also included in the .gitignore.
# To set a prefix from <prefixes.zsh>, just print a line to stdout that contains the same prefix definition
# you'd pass to this script as an argument.
#
# You can also have any directory structure inside <contents> or <symlinks>,
# which will be mirrored when syncing from one to the other.
# This is purely for your own organization, since each file itself stores where it points to.
#
# USAGE: config-repo.zsh <cmd> <args>
# (For more info see https://codeberg.org/T0mstone/config-repo)



### Util functions ###

not() {
	("$@") && return 1 || return 0
}

startswith() {
	local A="$1" B="$2"
	[[ "$A" = "$B"* ]] && declare -g LAST_SUFFIX="${A#"$B"}"
}

is-in() {
	local needle="$1"
	shift
	[[ "${argv[(i)$needle]}" -le $# ]]
}

call-normal() {
	"$@"
}

call-mute-err() {
	"$@" 2>/dev/null
}

pipe-to-file-with() {
	if [[ $# -eq 1 ]]
	then
		callfn=call-normal
	else
		callfn="$1"
		shift
	fi
	$callfn zsh -c "cat >${(qqq)1}"
}

### Library functions ###

validate-prefix-key() {
	local re='^[a-zA-Z0-9_]+$'

	local arg="$1"
	local err_info=""
	[[ ${+2} -eq 1 ]] && err_info=" (in prefix def ${(qqq)2})"
	[[ ${+3} -eq 1 ]] && err_info+=" (in argument no. $3)"

	if not grep -Eq "$re" <<<"$arg"
	then
		cat >&2 <<-EOF
			error: invalid prefix key ${(qqq)arg}$err_info
			↳ prefix key needs to match the following regex: $re
		EOF
		return 1
	fi
}

validate-prefix-value() {
	local re='^/.*$'

	local arg="$1"
	local err_info=""
	[[ ${+2} -eq 1 ]] && err_info=" (in prefix def ${(qqq)2})"
	[[ ${+3} -eq 1 ]] && err_info+=" (in argument no. $3)"

	if not grep -Eq "$re" <<<"$arg"
	then
		cat >&2 <<-EOF
			error: invalid prefix value ${(qqq)arg}$err_info
			↳ prefix value needs to match the following regex: $re
		EOF
		return 1
	fi
}

parse-prefix-def() {
	local PREFIX_REGEX='^(.+?)=(.+)$'

	local arg="$1"
	local argi="$2"
	local err_info=""
	[[ ${+2} -eq 1 ]] && err_info=" (argument no. $argi)"

	if not grep -Eq "$PREFIX_REGEX" <<<"$arg"
	then
		cat >&2 <<-EOF
			error: invalid prefix definition ${(qqq)arg}$err_info
			↳ prefix definition needs to match the following regex: $PREFIX_REGEX
		EOF
		return 1
	fi
	declare -g PREFIX_KEY="$(sed -E "s:$PREFIX_REGEX:\1:" <<<"$arg")"
	val="$(sed -E "s:$PREFIX_REGEX:\2:" <<<"$arg")"

	both_ok=true

	validate-prefix-key "$PREFIX_KEY" "$arg" $argi || both_ok=false
	validate-prefix-value "$val" "$arg" $argi || both_ok=false

	$both_ok || return 1

	if [[ "${val%/}" == "$val" ]]
	then
		val="$val/"
	fi

	declare -g PREFIX_VALUE="$val"
}

init-prefixes() {
	local -a prefix_defs=()

	if [[ -f ./prefixes.zsh ]]
	then
		local outp="$(zsh ./prefixes.zsh)"
		prefix_defs+=("${(f)outp}")
	fi
	# length of prefix defs from the file
	local before_cli=$#prefix_defs
	# put the cli args later so that they overwrite stuff from the file
	# (since they have higher priority)
	prefix_defs+=("$prefix_def_args[@]")

	declare -gA prefixes=()

	[[ $#prefix_defs -eq 0 ]] && return 0

	for ((i=1;i<=$#prefix_defs;i++))
	do
		pfx_def="$prefix_defs[$i]"
		unset j
		[[ $i -gt $before_cli ]] && j=$((i - before_cli))
		if parse-prefix-def "$pfx_def" $j
		then
			prefixes[$PREFIX_KEY]="$PREFIX_VALUE"
		fi
	done
}

eval-prefix() {
	[[ "${+prefixes}" = 0 ]] && {
		echo "internal error: called 'eval-prefix' before 'init-prefixes'" >&2
		return 3
	}

	zsh <<-EOF
		$(
			for pfx val in "${(@kv)prefixes}"
			do
				echo "$pfx=${(qqq)val}"
			done
		)
		# check if the variable is defined
		if (( \${+$1} ))
		then
			echo "known:\$$1"
		else
			echo "unknown"
		fi
	EOF
}

detect-prefix-and-split-path() {
	[[ "${+prefixes}" = 0 ]] && {
		echo "internal error: called 'detect-prefix-and-split-path' before 'init-prefixes'" >&2
		return 3
	}

	# note: the empty string is never a valid prefix name
	local res=""

	for pfx val in "${(@kv)prefixes}"
	do
		if startswith "$1" "$val"
		then
			# always prefer the more specific prefix
			if [[ "$#pfx" -gt "$#res" ]]
			then
				res="$pfx"
				declare -g REL_PATH="$LAST_SUFFIX"
			fi
		fi
	done

	if [[ "$res" != "" ]]
	then
		declare -g PREFIX="$res"
		return 0
	else
		return 1
	fi
}

# parse-arg.XXX are the different mix-and-match arg parsers that can be composed differently for each command
# they are always called as `parse-arg.XXX <arg value> <arg number>` for a single arg

parse-arg.prefix-def() {
	case "$1" in
	*=*)
		prefix_def_args+=("$arg")
		return 0
	;;
	esac
	return 1
}

parse-arg.positional() {
	if [[ $N_POSITIONAL -eq 0 ]]
	then
		return 1
	else
		((N_POSITIONAL-=1))
		POSITIONAL+=("$1")
		return 0
	fi
}

parse-arg.flags() {
	case "$1" in
		--*)
			flag="${1#--}"
			if [[ "${AVAILABLE_FLAGS[(r)$flag]}" == "$flag" ]]
			then
				FLAGS+=("$flag")
			else
				echo "error: unknown flag: --$flag [ignored]" >&2
			fi
		;;
		-*)
			for f in "${(@s::)1#-}"
			do
				if [[ ${+AVAILABLE_SHORT_FLAGS[$f]} -eq 0 ]]
				then
					echo "error: unknown short flag: -$f (from ${(qqq)1}) [ignored]" >&2
					continue
				fi

				FLAGS+=("$AVAILABLE_SHORT_FLAGS[$f]")
			done
		;;
		*)
			return 1
		;;
	esac
}

parse-args() {
	for ((i=1; i<=$#; i++))
	do
		arg="$argv[i]"
		arg_unhandled=true
		for fname in "$ARG_PARSERS[@]"
		do
			if "parse-arg.$fname" "$arg" "$i"
			then
				arg_unhandled=false
				break
			fi
		done
		if $arg_unhandled
		then
			echo "error: unexpected argument ${(qqq)arg} [ignored]" >&2
		fi
	done
}

parse-contents-file() {
	local contentpath="$1"

	local first_line="$(cat "$contentpath" | head -n 1)"
	local format_version_regex='^CONFIG-REPO-CONTENTS V([0-9]+) (.*)$'

	if not grep -qE "$format_version_regex" <<<"$first_line"
	then
		echo "error: file ${(qqq)contentpath} has invalid header format [file skipped]" >&2
		return 1
	fi

	local VALID_VERSIONS_STRING="the only valid version is 1"

	local format_version="$(sed -E "s|$format_version_regex|\1|" <<<"$first_line")"
	local first_line_rest="$(sed -E "s|$format_version_regex|\2|" <<<"$first_line")"

	case "$format_version" in
		1)
			local format_regex='PREFIX ([a-zA-Z0-9_-]+) RELPATH (.+)'
			if not grep -qE "$format_regex" <<<"$first_line_rest"
			then
				echo "error: file ${(qqq)contentpath} has invalid version 1 header format [file skipped]" >&2
				return 1
			fi

			declare -g PREFIX_KEY="$(sed -E "s|$format_regex|\1|" <<<"$first_line_rest")"
			declare -g REL_PATH="$(sed -E "s|$format_regex|\2|" <<<"$first_line_rest")"
			# sed '1d' removes the first line
			declare -g CONTENT="$(cat "$contentpath" | sed '1d')"
		;;
		*)
			echo "error: file ${(qqq)contentpath} declares unknown or future version or the config-repo contents format ($VALID_VERSIONS_STRING) [file skipped]" >&2
			return 1
		;;
	esac
}

### Initial setup and arg parsing ###

declare -a prefix_def_args=()

[[ $# -eq 0 ]] && {
	echo "error: no command provided (see \`$0 help\` for more info)" >&2
	return 2
}
cmd="$1"
shift

VERBOSE=false

### Main code ###

cd "$(dirname "$0")"

case "$cmd" in
help|--help|-h)
	echo "USAGE: config-repo.zsh <cmd> <args>"
	echo "(For more info see https://codeberg.org/T0mstone/config-repo)"
;;
init)
	ARG_PARSERS=()
	parse-args "$@"

	# init a config repo in this directory
	[[ ! -e ./.git ]] && git init
	mkdir -p ./contents
	mkdir -p ./symlinks
	cat >.gitignore <<-EOF
		/symlinks
		/prefixes.zsh
	EOF
	git add contents
	git add .gitignore
	git add "$0"
;;
sync-contents)
	typeset -a AVAILABLE_FLAGS=(verbose)
	typeset -A AVAILABLE_SHORT_FLAGS=(v verbose)
	typeset -a FLAGS=()
	ARG_PARSERS=(prefix-def flags)
	parse-args "$@"
	init-prefixes
	is-in verbose "$FLAGS[@]" && VERBOSE=true

	[[ ! -e ./contents ]] && mkdir ./contents

	# take files from <symlinks> and write the corresponding files into <contents>
	for linkpath in "${(@f)"$(find ./symlinks -type l)"}"
	do
		targetpath="$(readlink "$linkpath")"
		detect-prefix-and-split-path "$targetpath" || {
			echo "error: path ${(qqq)targetpath} doesn't start with any known prefix" >&2
			return 1
		}
		contentpath="./contents/${linkpath#./symlinks/}"
		mkdir -p "$(dirname "$contentpath")"
		printf "%s\n" "CONFIG-REPO-CONTENTS V1 PREFIX $PREFIX RELPATH $REL_PATH" >"$contentpath" \
			&& $VERBOSE && echo "wrote path from symlink ${(qqq)linkpath} to ${(qqq)contentpath}"
		cat "$targetpath" >>"$contentpath" \
			&& $VERBOSE && echo "wrote content of ${(qqq)targetpath} to ${(qqq)contentpath}"
	done

	# check for files/dirs in <contents> that aren't in <symlinks>
	for contentpath in "${(@f)"$(find ./contents -type f,d)"}"
	do
		linkpath="./symlinks${contentpath#./contents}"
		[[ -d "$contentpath" ]] && kind=("directory" "directory" -d) || kind=("file" "symlink" -L)
		if eval "[[ ! $kind[3] \"\$linkpath\" ]]"
		then
			echo "warning: $kind[1] ${(qqq)contentpath} doesn't have a corresponding $kind[2] in \"./symlinks\", you might want to delete it" >&2
			# TODO: prompt the user to delete it
		fi
	done
;;
sync-symlinks)
	typeset -a AVAILABLE_FLAGS=(verbose no-sync write-contents make-dirs try-sudo)
	typeset -A AVAILABLE_SHORT_FLAGS=(v verbose n no-sync w write-contents p make-dirs S try-sudo)
	typeset -a FLAGS=()
	ARG_PARSERS=(prefix-def flags)
	parse-args "$@"
	init-prefixes
	is-in verbose "$FLAGS[@]" && VERBOSE=true
	is-in no-sync "$FLAGS[@]" && DO_SYNC=false || DO_SYNC=true
	is-in write-contents "$FLAGS[@]" && DO_WRITE=true || DO_WRITE=false
	is-in make-dirs "$FLAGS[@]" && MAKE_DIRS=true || MAKE_DIRS=false
	is-in try-sudo "$FLAGS[@]" && TRY_SUDO=true || TRY_SUDO=false

	[[ ! -e ./symlinks ]] && mkdir ./symlinks

	for contentpath in "${(@f)"$(find ./contents -type f)"}"
	do
		linkpath="./symlinks/${contentpath#./contents/}"

		parse-contents-file "$contentpath" || continue

		if ((${+PREFIX_KEY} + ${+REL_PATH} + ${+CONTENT} < 3))
		then
			echo "internal error: parse-contents-file failed to set all results (for file ${(qqq)contentpath}) [file skipped]" >&2
			continue
		fi

		if not validate-prefix-key "$PREFIX_KEY" 2>/dev/null
		then
			echo "error: file ${(qqq)contentpath} has invalid prefix name [file skipped]" >&2
			continue
		fi

		PREFIX_VALUE="$(eval-prefix "$PREFIX_KEY")"

		case "$PREFIX_VALUE" in
			known:*) PREFIX_VALUE="${PREFIX_VALUE#known:}" ;;
			unknown)
				echo "error: file ${(qqq)contentpath} has unknown prefix name ${(qqq)PREFIX_KEY} [file skipped]" >&2
				continue
			;;
			*)
				echo "internal error: eval-prefix returned wrong format (for file ${(qqq)contentpath}) [file skipped]" >&2
				continue
		esac

		targetpath="$PREFIX_VALUE$REL_PATH"

		if [[ -e "$linkpath" && ! -L "$linkpath" ]]
		then
			echo "error: file ${(qqq)linkpath} already exists but is not a symlink [file skipped]" >&2
			continue
		fi

		$DO_SYNC && {
			mkdir -p "$(dirname "$linkpath")"
			ln -sf "$targetpath" "$linkpath" \
				&& $VERBOSE && echo "wrote path from ${(qqq)contentpath} to ${(qqq)linkpath}"
		}

		$DO_WRITE && {
			succ=false

			$MAKE_DIRS && mkdir -p "$(dirname "$targetpath")"

			# don't display the initial write error, only if we'll try again with sudo afterwards (otherwise it would be confusing to the user)
			$TRY_SUDO && callfn=call-mute-err || callfn=call-normal

			if printf "%s\n" "$CONTENT" | pipe-to-file-with $callfn "$targetpath"
			then
				succ=true
			else
				$TRY_SUDO && {
					$VERBOSE && echo "failed to write data from ${(qqq)contentpath} to ${(qqq)targetpath} (now trying with sudo)"
					printf "%s\n" "$CONTENT" | pipe-to-file-with sudo "$targetpath" && succ=true
				}
			fi
			$VERBOSE && {
				if $succ
				then echo "wrote data from ${(qqq)contentpath} to ${(qqq)targetpath}"
				else echo "failed to write data from ${(qqq)contentpath} to ${(qqq)targetpath}"
				fi
			}
		}

		# to say something when we're doing nothing
		not $DO_SYNC && not $DO_WRITE && $VERBOSE && echo "done nothing to file ${(qqq)contentpath}"
	done

	# check for files/dirs in <symlinks> that aren't in <contents>
	for linkpath in "${(@f)"$(find ./symlinks -type l,d)"}"
	do
		contentpath="./contents${linkpath#./symlinks}"
		[[ -d "$linkpath" ]] && kind=("directory" "directory" -d) || kind=("symlink" "file" -f)
		if eval "[[ ! $kind[3] \"\$contentpath\" ]]"
		then
			echo "warning: $kind[1] ${(qqq)linkpath} doesn't have a corresponding $kind[2] in \"./contents\", you might want to delete it" >&2
			# TODO: prompt the user to delete it
		fi
	done
;;
self-update)
	typeset -a AVAILABLE_FLAGS=(verbose source)
	typeset -A AVAILABLE_SHORT_FLAGS=(v verbose s source)
	typeset -a FLAGS=()
	N_POSITIONAL=2
	declare -a POSITIONAL=()
	ARG_PARSERS=(flags positional)
	parse-args "$@"
	is-in verbose "$FLAGS[@]" && VERBOSE=true

	case "$#POSITIONAL" in
		2) SOURCE="$POSITIONAL[1]"; REV="$POSITIONAL[2]" ;;
		1) if is-in source "$FLAGS[@]"; then SOURCE="$POSITIONAL[1]"; else REV="$POSITIONAL[1]"; fi ;;
		*) SOURCE='https://codeberg.org/T0mstone/config-repo' ;;
	esac

	git clone --depth=1 "$SOURCE" .update
	if [[ "${+REV}" -eq 1 ]]
	then
		# check out a specific ref (as opposed to main)
		cd .update
		git checkout "$REV"
		$VERBOSE && {
			local ref="$(git describe --all --exact-match)"
			case "$ref" in
				heads/main)
					echo "updating to state of main branch."
				;;
				heads/*)
					echo "updating to state of branch ${ref#heads/}."
				;;
				tags/*)
					echo "updating to state of tag ${ref#tags/}."
				;;
				fatal:*)
					echo "updating to state of commit $(git rev-parse HEAD --short)."
				;;
			esac
		}
		cd ..
	else
		$VERBOSE && echo "updating to state of main branch."
	fi
	exec .update/update-helper.sh .update "$0"
;;
esac