# `config-repo`

This is a Shell script to help you make your system more easily reinstallable.

You can find a detailed explanation of how it works here (see [Explanation](#explanation)) or at the start of the file itself.

## Shell compatibility

This is tested to work with zsh 5.8.

I won't actively support any other shells,
but you're welcome to open a PR that
- adds a shell to this section if the original file works with it
- adds a port of the file to another shell

# Explanation

The script maintains a directory structure as follows:
```
(base directory)
	├ config-repo.zsh
	├ (prefixes.zsh)
	├ .git
	├ .gitignore
	├ contents
	│	├ fileA
	│	├ fileB
	│	└ ...
	└ symlinks
		├ fileA
		├ fileB
		└ ...
```
Each of the files in `symlinks` is a symlink to some user/system config file
(or anything really, but it's intended for config files).
To maintain privacy, the `symlinks` directory is included in the .gitignore,
so that it's possible to publish the repo without leaking local paths.

Instead, the files in `contents` have a special format where the first line of the file
contains a prefix (some variable you can set) and a relative path.
This approach to only storing local paths keeps everything nice and anonymous
(and also more portable).

Prefixes are simply set as arguments to the script
- Valid prefix names are strings made of ascii alphanumeric characters and/or `_`.
- Valid prefix values are strings starting with `/` (absolute paths).

The script itself has to be in the same directory (also tracked by git) so that it can be shared,
and doing this also helps add clarity as to what the directory is for and how it works
(e.g. in case you're revisiting it after 6 months and have forgotten about it in the meantime).

Optionally, you can have the `prefixes.zsh` file set the prefixes, so you don't have to set them manually.
That file is also included in the .gitignore.
To set a prefix from `prefixes.zsh`, just print a line to stdout that contains the same prefix definition
you'd pass to this script as an argument.

You can also have any directory structure inside `contents` or `symlinks`,
which will be mirrored when syncing from one to the other.
This is purely for your own organization, since each file itself stores where it points to.

## Usage

The first argument is interpreted as the subcommand to execute (like how `git` does it).

There are currently 4 subcommands:

- `init`:
This initializes an empty git repo in the script's directory (if there isn't one already),
adds the `symlinks` and `contents` directories (if they don't exist)
and creates/overwrites [`.gitignore`](#gitignore-contents-after-init).
	- This subcommand doesn't allow any arguments
- `sync-contents`:
This looks at all symlinks in `symlinks` and, for each of them,
takes the path it points to, tries to match that to a known prefix (errors if that fails)
and stores the found prefix and relative path as well as the content of the symlink's target file
into a file in `contents` with the same name as the symlink.
	- This subcommand allows prefix definition arguments and the following flag arguments:
		- `-v` / `--verbose`: enables verbose mode
	- If multiple prefixes apply, the most specific (i.e. the longest) is chosen
- `sync-symlinks`:
This looks at all files in `contents` and, for each of them,
combines the contained prefix (errors if it's unknown) and relative path
and creates/overwrites a symlink in the `symlinks` directory with the same name as the looked-at file
so that it points to the given target.
	- This subcommand allows prefix definition arguments and the following flag arguments:
		- `-v` / `--verbose`: enables verbose mode
		- `-w` / `--write-contents`: Also writes the contents from the looked-at file to the target file
		- `-p` / `--make-dirs`: If `-w`/`--write-contents` is present, also creates all directories containing the target path if they don't already exist (using `mkdir -p`)
		- `-n` / `--no-sync`: Doesn't write anything to the `symlinks` directory.
		This can be useful in combination with `-w` to only write the contents,
		or alone as a sort of dry-run mode.
		- `-S` / `--try-sudo`: If a target file can't be written to, try again with `sudo`.
		This only applies to _target_ files (i.e. it only works together with `-w`)
		and can be useful for installing system files that belong to `root`.
- `self-update`:
This clones the source repo (usually this repo, i.e. https://codeberg.org/T0mstone/config-repo) into a subdirectory called `.update`
and then executes `.update/update-helper.sh`, which copies `.update/config-repo.zsh` to `config-repo.zsh`
and then deletes `.update`.
	- This subcommand allows positional arguments and the following flag arguments:
		- `-v` / `--verbose`: enables verbose mode
		- `-s` / `--source`: when one positional argument is given, interpret it as the first argument instead of the second
	- Positional arguments:
		- (optional) the source repo to clone from
		- (optional) the git rev (e.g. branch, tag, commit hash) to update to

### Types of arguments

Each command states what arguments it can accept and, for a given command,
only the stated types of arguments are checked (others will error as 'unexpected argument').

- If prefix definition arguments are checked, they take priority over other types.
All argument that contain `=` are then considered prefix definitions.
- If flag arguments are checked, all arguments that start with `-` (short flags) or `--` (long flags) are considered flags
	- short flags can be combined like usual, so passing one argument `-ab` is equivalent to passing two arguments `-a -b`
	- all short flags have an equivalent long flag
	- Passing a flag multiple times is allowed, but all occurrences past the first one are ignored by default (unless the docs for the command say otherwise).
- If positional arguments are checked, a limited number of arguments that aren't any of the above will be accepted
	- They can be optional or required; In the latter case an error will occur if too few arguments were given

### .gitignore contents after `init`

```.gitignore
/symlinks
/prefixes.zsh
```

## Examples / Common actions

Clone a config repo from online and init it:
```shell
git clone <url>
cd <dirname>
./config-repo.zsh sync-symlinks
```

Example: Clone a config repo that only uses files with prefix `local`
and install its files into `~/.local`
```shell
git clone <url>
cd <dirname>
./config-repo.zsh sync-symlinks
./config-repo.zsh write-symlinks local=~/.local
```

Update the `contents` directory and commit and push it
```shell
./config-repo.zsh sync-contents
git add contents/*
git commit
git push
```

Update `config-repo.zsh` to the latest version
```shell
./config-repo.zsh self-update
```