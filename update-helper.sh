#!/usr/bin/env sh

update_dir="$1"
target_path="$2"

case "$target_path" in
*/config-repo.zsh)
	cp "$update_dir/config-repo.zsh" "$target_path"
	rm -rf "$update_dir"
;;
*)
	# should never happen when this is called by config-repo.zsh
	echo "error: can't update file that isn't config-repo.zsh" >&2
	return 1
;;
esac